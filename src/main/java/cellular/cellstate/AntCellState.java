package cellular.cellstate;

import java.awt.Color;

public class AntCellState extends CellState {
	public AntCellState() {
		super(3);

	}

	@Override
	public Color getColor() {
		return Color.yellow;
	}

	@Override
	public int getValue() {
		return -1;
	}

}
